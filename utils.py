from typing import Callable
import json
from langchain_core.messages import ToolMessage
from langchain_core.runnables import RunnableLambda
from langgraph.prebuilt import ToolNode

from state import State
from constant import PRICE_OPENAI, USD2VND

def handle_tool_error(state) -> dict:
    error = state.get("error")
    tool_calls = state["messages"][-1].tool_calls
    return {
        "messages": [
            ToolMessage(
                content=f"Error: {repr(error)}\n please fix your mistakes.",
                tool_call_id=tc["id"],
            )
            for tc in tool_calls
        ]
    }


def create_tool_node_with_fallback(tools: list) -> dict:
    return ToolNode(tools).with_fallbacks(
        [RunnableLambda(handle_tool_error)], exception_key="error"
    )



def create_entry_node(assistant_name: str, new_dialog_state: str) -> Callable:
    def entry_node(state: State) -> dict:
        tool_call_id = state["messages"][-1].tool_calls[0]["id"]
        return {
            "messages": [
                ToolMessage(
                    content=f"The assistant is now the {assistant_name}. Reflect on the above conversation between the host assistant and the user."
                    f" The user's intent is unsatisfied. Use the provided tools to assist the user. Remember, you are {assistant_name},"
                    " and the search tour, order tour, other other action is not complete until after you have successfully invoked the appropriate tool."
                    " If the user changes their mind or needs help for other tasks, call the CompleteOrEscalate function to let the primary host assistant take control."
                    " Do not mention who you are - just act as the proxy for the assistant.",
                    tool_call_id=tool_call_id,
                )
            ],
            "dialog_state": new_dialog_state,
        }

    return entry_node

def merge_spaces(text):
    # Merges multiple spaces in a string into a single space.
    return ' '.join(text.split())

def caculate_tokens_and_price(ai_response: dict, price_map: dict = PRICE_OPENAI, usd2vnd: float = USD2VND):
    total_cost = 0
    total_tokens = 0
    for mess in ai_response['messages']:
        if mess.type == 'ai':
            input_tokens = mess.usage_metadata['input_tokens']
            output_tokens = mess.usage_metadata['output_tokens']
            total_tokens = total_tokens + input_tokens + output_tokens
            model_name = mess.response_metadata['model_name']
            cost = input_tokens * price_map[model_name]["input"] + output_tokens * price_map[model_name]["output"]
            total_cost = total_cost + cost

    total_cost_vnd = total_cost * usd2vnd
    return total_tokens, total_cost_vnd

def check_keys_in_json(json_data, required_keys: list):
    """
    Kiểm tra xem một JSON có đầy đủ các key cần thiết hay không.

    Args:
    json_data: Dữ liệu JSON dưới dạng dictionary hoặc chuỗi.
    required_keys: Danh sách các key cần kiểm tra.

    Returns:
    True nếu JSON có đầy đủ các key, False nếu không.
    """

    # Đảm bảo dữ liệu là một dictionary
    if isinstance(json_data, str):
        json_data = json.loads(json_data)

    # Kiểm tra xem tất cả các key cần thiết có trong JSON hay không
    return all(key in json_data for key in required_keys)