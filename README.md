# Travel Assistant LangGraph

## Giới thiệu


## Tính năng
- Đồng bộ dữ liệu real time từ RDBMS 
- Hỗ trợ tải tri thức từ các định dạng file: PDF, Microsoft Ofice (docx, xlsx, pptx)

## Getting started

```
sudo wget -P ./neo4j/plugins/  https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/4.4.0.28/apoc-4.4.0.28-all.jar 
```

Create a `.env` file in the top level directory (i.e. travel_mapper) that contains the following lines
```
PYTHONPATH=path/to/project

OPENAI_API_KEY= ...
NEO4J_USERNAME= neo4j
NEO4J_PASSWORD= ...
NEO4J_URI= bolt://localhost:7687
```

Start docker compose for db:
```
docker-compose up -d
```

Import data to Neo4j
```
python ingest.py
```

Start ngrok for domain
```
ngrok http --domain=gar-apt-crane.ngrok-free.app 1905
```

Start fastapi app
```
uvicorn main_app:app --host 0.0.0.0 --port 1905 --reload
```

Start api server
```
uvicorn server_api:app --host 0.0.0.0 --port 1906 --reload
```

Note: <br>
- Chua xu ly truong hop mat ket noi internet