from pathlib import Path
from dotenv import load_dotenv

load_dotenv()

BASE_DB_DIR = Path(__file__).parents[0]
DB_INIT_FILE = BASE_DB_DIR / 'database.ini'