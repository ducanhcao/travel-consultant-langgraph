CREATE EXTENSION IF NOT EXISTS vector;

CREATE TABLE IF NOT EXISTS tours (
  id BIGINT PRIMARY KEY, -- Unique ID for each tour.
  tour_name VARCHAR(255), -- Name of the tour.
  tour_description TEXT, -- Overview of what will happen on the tour.
  description_embedding VECTOR, -- Embedding of tour_description for similar search purposes.
  tour_itinerary TEXT, -- Details of destinations and activities that will take place on each day.
  tour_policy TEXT, -- Tour policies, tour price includes and excludes services, cancellation policies, ...
  departures VARCHAR(1024), -- Departures location, usually city name and country.
  departures_embedding VECTOR, -- Embedding of departure_name for similar search purposes.
  destinations VARCHAR(1024), -- Destinations location, usually city name and country.
  destinations_embedding VECTOR, -- Embedding of destination_name for similar search purposes.
  adult_price VARCHAR(20), -- Tour cost per adult.
  other_price_fomulas TEXT, -- Details on how pricing is calculated, including detailed pricing for each age group.
  document_url VARCHAR(1024), -- Link to the most detailed and complete tour program document.
  document_file_type INT, -- Document file format, for example 1 is html, 2 is doc/docx/pdf.
  tour_type INT, -- Tour classification code.
  tour_status INT, -- Tour status code to determine the tour's open sale status.
  start_time BIGINT, -- Tour departure date by timestamp.
  number_days INT, -- Number of days the tour takes place.
  number_nights INT, -- Number of nights the tour takes place.
  slots_available INT, -- Number of slots available for the tour.
  max_participants INT -- Maximum capacity of tour.
);
