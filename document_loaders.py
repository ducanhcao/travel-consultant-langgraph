from spire.doc import *
from spire.doc.common import *
import PyPDF2

def read_content_pdf(file_path):
    content = ""
    with open(file_path, mode='rb') as pdf_file:
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        num_pages = len(pdf_reader.pages)
        for page_num in range(num_pages):
            page = pdf_reader.pages[page_num]
            content += page.extract_text()
    
    return content

def read_content_docx_and_doc(file_path):
    # Create a Document object
    document = Document()
    # Load a Word document
    document.LoadFromFile(file_path)

    # Extract the text of the document
    document_text = document.GetText()
    content = document_text.replace("Evaluation Warning: The document was created with Spire.Doc for Python.", "")
    
    return content