import json

PRICE_OPENAI = {
    "gpt-4o-mini-2024-07-18": {
        "input": 0.150/1000000,
        "output": 0.600/1000000
    },
}
USD2VND = 25142.50

PGDB_QUERY_MATCH = {
    "description": {
        "similarity": "(1 - (description_embedding <=> %(description_embedding)s))",
        "weight": 0.2
    },
    "departure": {
        "similarity": "(1 - (departures_embedding <=> %(departure_embedding)s))",
        "weight": 0.4
    },
    "destination": {
        "similarity": "(1 - (destinations_embedding <=> %(destination_embedding)s))",
        "weight": 0.4
    }
}

SEARCH_TOUR_PGVECTOR_SYSTEM_PROMPT = f'''
    You are an advanced language model trained to extract structured information from natural language user inputs. Given a user query related to searching for tours in a PostgreSQL database, your task is to identify and extract specific pieces of information relevant to the search criteria. You need to identify the following entities from the user's request:

    description: Any specific activities, features, or descriptions that the user mentions they are looking for in the tour.
    destination: The place or places the user wants to visit.
    departure: The starting location or city where the user wants to begin the tour.
    maximum_cost: The highest amount the user is willing to pay per person for the tour.
    Your goal is to analyze the user's input, identify any of the above entities mentioned, and output them in a structured JSON format.
    
    Example user input:
    "có tour nào từ Hồ Chí Minh đi Phú Quốc có cáp treo không, chi phí tối đa 10tr 1 người"
    
    There are four relationships to analyse:
    1. Mentioning "đi Phú Quốc" means the destination is "Phú Quốc"
    2. The mention of "từ Hồ Chí Minh" means the departure point is "Hồ Chí Minh"
    3. Mentioning "đi Phú Quốc có cáp treo" means the user wants to "đi du lịch Phú Quốc, nơi có hoạt động đi cáp treo"
    4. Mentioning "chi phí tối đa 10tr 1 người" means maximum cost is 10000000
    
    
    Return a json object following the following rules:
    For each relationship to analyse, add a key value pair with the key being an exact match for one of the entity types provided, and the value being the value relevant to the user query.
    
    For the example provided, the expected output would be:
    {{
        "Description": "đi du lịch Phú Quốc, nơi có hoạt động đi cáp treo",
        "destination": "Phú Quốc'",
        "departure": "Hồ Chí Minh",
        "maximum_cost": "10000000",
    }}
    
    If there are no relevant entities in the user prompt, return an empty json object.
'''

GRAPHDB_ENTITY_TYPES = {
    "Description": "User's wishes for that tour",
    "Destination": "The place where the tour will go'",
    "Departure": "Departure location of the tour",
    "Maximum_cost": "The maximum amount a user can pay for a tour",
}

RELATION_TYPES = {
    "goTo": "the tour will go to this location",
    "startFrom": "the tour will depart from this location",
    "hasPricePerPerson": "the tour has a price per person",
}

ENTITY_RELATIONSHIP_MATCH = {
    "Destination": "GO_TO",
    "Departure": "START_FROM",
}
ENTITY_PROPERTIES_EMBED = {
    "Description": "description",
}
ENTITY_PROPERTIES_FILTER = {
    "Maximum_cost": "adult_price",
}

QUERY_GRAPHDB_SYSTEM_PROMPT = f'''
    You are a helpful agent designed to fetch information from a graph database. 
    
    The graph database links tours to the following entity types:
    {json.dumps(GRAPHDB_ENTITY_TYPES)}
    
    Each link has one of the following relationships:
    {json.dumps(RELATION_TYPES)}

    Depending on the user prompt, determine if it possible to answer with the graph database.
        
    The graph database can match products with multiple relationships to several entities.
    
    Example user input:
    "có tour nào từ Hồ Chí Minh đi Phú Quốc có cáp treo không, chi phí tối đa 10tr 1 người"
    
    There are four relationships to analyse:
    1. Mentioning "đi Phú Quốc" means the destination is "Phú Quốc"
    2. The mention of "từ Hồ Chí Minh" means the departure point is "Hồ Chí Minh"
    3. Mentioning "đi Phú Quốc có cáp treo" means the user wants to "đi du lịch Phú Quốc, nơi có hoạt động đi cáp treo"
    4. Mentioning "chi phí tối đa 10tr 1 người" means maximum cost is 10000000
    
    
    Return a json object following the following rules:
    For each relationship to analyse, add a key value pair with the key being an exact match for one of the entity types provided, and the value being the value relevant to the user query.
    
    For the example provided, the expected output would be:
    {{
        "Description": "đi du lịch Phú Quốc, nơi có hoạt động đi cáp treo",
        "Destination": "Phú Quốc'",
        "Departure": "Hồ Chí Minh",
        "Maximum_cost": "10000000",
    }}
    
    If there are no relevant entities in the user prompt, return an empty json object.
'''

CONTENT_TYPE_MATCH_TMP_FILE = {
    "application/msword": "temp_file.doc",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document": "temp_file.docx",
    "application/pdf": "temp_file.pdf"
}

EXTRACT_INFO_TOUR_SYSTEM_PROMPT = """
You are an intelligent travel assistant. I will provide you with a detailed text describing a tour. Your task is to analyze this text and return the following information in JSON format:

1. **tour_summary**: A brief description of the key highlights of the tour. Summarize what the customers will experience, such as key attractions, activities, special experiences (e.g., cable car rides, 5-star hotels, cruises, etc.). This should be concise and easy for customers to understand.

2. **tour_itinerary**: A detailed day-by-day itinerary for the tour, including specific activities, places to visit, and times for each activity.

3. **tour_policy**: The tour's policies, including what's included, what's not included, any important notes, and any additional information that customers should be aware of.

Must stick to the information provided by the user, no fabrication, no creativity.

Please return the result in the following JSON format:
{"tour_summary": "string summary text here",
"tour_itinerary": "string detailed schedule by day here",
"tour_policy": "string policy text here"}
Required information must be in the same language as the information provided by the user.
Only return JSON and nothing else is allowed.
"""

