import os 
from typing import Annotated, Literal, Optional, TypedDict
import operator
from langchain_core.messages import AnyMessage, SystemMessage, HumanMessage, ToolMessage
from langchain_openai import ChatOpenAI
from langchain_core.tools import tool
from langchain_core.runnables import ensure_config
from typing import Any
from langchain_core.prompts import ChatPromptTemplate
from langchain_core.runnables import Runnable, RunnableConfig
from langchain_core.pydantic_v1 import BaseModel, Field
from datetime import datetime
from langchain_community.tools.tavily_search import TavilySearchResults
from langgraph.graph import END, StateGraph, START
from langgraph.prebuilt import tools_condition
from langgraph.checkpoint.sqlite import SqliteSaver
from openai import OpenAI

from utils import merge_spaces
from constant import *
from utils import create_tool_node_with_fallback, create_entry_node, caculate_tokens_and_price
from state import State
from bot_config import salesperson_name, company_name
# from graphrag import GraphStoreRetriever
from pgvector_rag import PgvectorRetriever

from dotenv import load_dotenv
_ = load_dotenv()

# graph_retriever = GraphStoreRetriever.from_neo4j(db_url=os.getenv("NEO4J_URI"), \
#                                                 db_username=os.getenv("NEO4J_USERNAME"), \
#                                                 db_password=os.getenv("NEO4J_PASSWORD"), \
#                                                 db_database=os.getenv("NEO4J_DATABASE"), \
#                                                 openai_embed_model="text-embedding-3-large")

pgdb_retriever = PgvectorRetriever.from_openai(openai_client=OpenAI(), openai_embed_model="text-embedding-3-large")

@tool
def search_tour_by_user_needs(query: str) -> str:
    """Search for tours that match user requirements.
    
    Args:
        query (str): Description of the tour that the user wants.

    Returns:
        str: List of tours suitable to user needs.
    """
    tours = pgdb_retriever.query(query)
    return tours

@tool
def get_all_tour() -> str:
    """Get all the tour names you have.

    Returns:
        str: List of all tours you have.
    """
    tours = pgdb_retriever.get_all_tour()
    return tours

@tool 
def get_tour_details_by_id(tour_id: str) -> str:
    """Get tour details by specific id
    Args:
        tour_id (str): id of tour

    Returns:
        str: Detailed information of a specific tour includes tour program, tour price, number of seats available, departure date, tour policy, tour inclusions and exclusions, ...
    """
    tour = pgdb_retriever.get_tour_by_id(tour_id)
    return tour

@tool
def connect_tour_sales_staff(thread_id: str, user_id: str) -> str:
    """Assign a sales person to assist customers with booking tours, negotiating, and resolving queries.
    
    Args:
        thread_id (str): Current thread_id in config.
        user_id (str): Current user_id in config.

    Returns:
        str: List of tours suitable to user needs.
    """
    print(f"\n\nTINGGGGGGGGGGGGGGGGGGG !!!!!!!!!!!!!!!!!!!!!!!!! thread_id: {thread_id}, user_id: {user_id}\n\n")
    return "Connecting to a sales person. Please reply to the user and wait a moment."


@tool
def fetch_user_information() -> dict:
    """Get user information.

    Returns:
        Dictionary containing detailed information about the user.
    """
    config = ensure_config()  # Fetch from the context
    configuration = config.get("configurable", {})
    user_id = configuration.get("user_id", None) 
    if not user_id:
        raise ValueError("No user ID configured.")
    return {
        "user_name": configuration.get("user_name"),
        "user_phone_number": configuration.get("user_phone_number")
    }

# AGENT
# Define assistant function -----------------------------------------------------------------------------------
class Assistant:
    def __init__(self, runnable: Runnable):
        self.runnable = runnable

    def __call__(self, state: State, config: RunnableConfig):
        while True:
            result = self.runnable.invoke(state)
            # If the LLM happens to return an empty response, we will re-prompt it
            # for an actual response.
            if not result.tool_calls and (
                not result.content
                or isinstance(result.content, list)
                and not result.content[0].get("text")
            ):
                messages = state["messages"] + [("user", "Respond with a real output.")]
                state = {**state, "messages": messages}
            else:
                break
        return {"messages": result}
    
class CompleteOrEscalate(BaseModel):
    """A tool to mark the current task as completed and/or to escalate control of the dialog to the main assistant,
    who can re-route the dialog based on the user's needs."""

    cancel: bool = True
    reason: str

    class Config:
        schema_extra = {
            "example": {
                "cancel": True,
                "reason": "User changed their mind about the current task.",
            },
            "example 2": {
                "cancel": True,
                "reason": "I have fully completed the task.",
            },
            "example 3": {
                "cancel": False,
                "reason": "I need to search the tour for more information.",
            },
        }

def user_info(state: State) -> dict:
    return {"user_info": fetch_user_information.invoke({})}

# This node will be shared for exiting all specialized assistants ------------------------------------------------
def pop_dialog_state(state: State) -> dict:
    """Pop the dialog stack and return to the main assistant.

    This lets the full graph explicitly track the dialog flow and delegate control
    to specific sub-graphs.
    """
    messages = []
    if state["messages"][-1].tool_calls:
        # Note: Doesn't currently handle the edge case where the llm performs parallel tool calls
        messages.append(
            ToolMessage(
                content="Resuming dialog with the host assistant. Please reflect on the past conversation and assist the user as needed.",
                tool_call_id=state["messages"][-1].tool_calls[0]["id"],
            )
        )
    return {
        "dialog_state": "pop",
        "messages": messages,
    }

class ToTourConsultantAssistant(BaseModel):
    """Transfers work to highly knowledgeable and friendly tour consultant assistant to assisting customers in finding the perfect tour"""

    query: str = Field(
        description="User's message"
    )


class ToTourSaleAssistant(BaseModel):
    """Transfers work to proficient and personable tour sales specialist to assisting customers in booking the tour
    Key tasks:
    - Negotiate tour prices with customers, apply promotional policies, and adjust tour designs to match the customer's budget. 
    - Present and explain additional services that customers can purchase to enhance their tour experience. 
    - Assist customers through the tour booking process, ensuring all their needs and preferences are met. 
    """

    query: str = Field(
        description="Customer's desire for a specific tour. For example: want to reduce tour price."
    )

tour_consultant_tools = [search_tour_by_user_needs, get_all_tour, get_tour_details_by_id, connect_tour_sales_staff]

def route_tour_consultant(
    state: State,
) -> Literal[
    "tour_consultant_tools",
    "leave_skill",
    "__end__",
]:
    route = tools_condition(state)
    if route == END:
        print("route_tour_consultant: END")
        return END
    tool_calls = state["messages"][-1].tool_calls
    did_cancel = any(tc["name"] == CompleteOrEscalate.__name__ for tc in tool_calls)
    if did_cancel:
        print("route_tour_consultant: leave_skill")
        return "leave_skill"
    toolnames = [t.name for t in tour_consultant_tools]
    if all(tc["name"] in toolnames for tc in tool_calls):
        return "tour_consultant_tools"

tour_sale_tools = [connect_tour_sales_staff]

def route_tour_sale(
    state: State,
) -> Literal[
    "tour_sale_tools",
    "leave_skill",
    "__end__",
]:
    route = tools_condition(state)
    if route == END:
        return END
    tool_calls = state["messages"][-1].tool_calls
    did_cancel = any(tc["name"] == CompleteOrEscalate.__name__ for tc in tool_calls)
    if did_cancel:
        return "leave_skill"
    toolnames = [t.name for t in tour_sale_tools]
    if all(tc["name"] in toolnames for tc in tool_calls):
        return "tour_sale_tools"

def route_primary_assistant(
    state: State,
) -> Literal[
    "enter_tour_consultant",
    "enter_tour_sale",
    "__end__",
]:
    route = tools_condition(state)
    if route == END:
        return END
    tool_calls = state["messages"][-1].tool_calls
    if tool_calls:
        print(tool_calls[0]["name"])
        if tool_calls[0]["name"] == ToTourConsultantAssistant.__name__:
            return "enter_tour_consultant"
        elif tool_calls[0]["name"] == ToTourSaleAssistant.__name__:
            return "enter_tour_sale"
    raise ValueError("Invalid route")

# Each delegated workflow can directly respond to the user
# When the user responds, we want to return to the currently active workflow
def route_to_workflow(
    state: State,
) -> Literal[
    "primary_assistant",
    "tour_consultant",
    "tour_sales_manager",
]:
    """If we are in a delegated state, route directly to the appropriate assistant."""
    dialog_state = state.get("dialog_state")
    if not dialog_state:
        return "primary_assistant"
    return dialog_state[-1]

class GraphAgent:
    def __init__(self, graph):
        self.graph = graph
    
    @classmethod
    def from_llm_with_memory(cls, memory, llm=ChatOpenAI(model="gpt-4o-mini", temperature=0.1)) -> None:
        tour_consultant_prompt = ChatPromptTemplate.from_messages(
            [
                (
                    "system",
                    "You are a highly knowledgeable and friendly travel consultant. "
                    "Your goal is to assist customers in finding the perfect tour for their needs. " 
                    "\nSuggest suitable tours, while ensuring a seamless experience for the customer. "
                    "\nPay special attention to the number of available seats on the tour and the number of people the customer "
                    "wants to register for. If there are no more seats on the tour, you must find another tour with similar departure "
                    "and destination points to suggest to the customer. If you cannot find a similar tour, please contact the "
                    "support staff by calling the connect_tour_sales_staff tool. "
                    "\nIf a customer asks for information that you don't have, the tool's results don't mention it, and you don't have any data or "
                    "basis to answer that question, then immediately transfer it to a support staff by calling the connect_tour_sales_staff tool. "
                    "But before that, be sure to double-check the information from the chat history and the tool's results to "
                    "make sure you don't have enough information."
                    "\nAfter providing an answer, ask further open-ended questions to get more information about the customer including "
                    "their expected cost, number of participants, desired departure date if they have not previously provided this information. "
                    "\nBe polite, enthusiastic, and responsive to their needs. " 
                    "\n\nExample 1: "
                    "\n- User: Is there any tour to Nha Trang, departing from Hanoi? I need a 5-star hotel and a cruise. "
                    "\n- You must use the search_tour_by_user_needs tool. "
                    "\nExample 2: "
                    "\n- User: What tours do you have? "
                    "\n- You must use the get_all_tour tool. "
                    "\nExample 3: "
                    "\n- User: Do you have any tour to Da Nang? "
                    "\n- You must use the search_tour_by_user_needs tool. "
                    "\nExample 4: "
                    "\n- User: I need tour information 34058234234 "
                    "\n- You must use the get_tour_details_by_id tool and pass in tour id 34058234234. "
                    "\nExample 5: "
                    "\n- User: Give me that tour information "
                    "\n- You must review the chat history to see which tour the user is interested in, then use the get_tour_details_by_id tool. "
                    "\nExample 6: "
                    "\n- User: What star hotel is that tour at? "
                    "\n- You check the chat history, get information about the tour that the customer is interested in, if it mentions "
                    "the hotel star rating, reply to the customer, if not, call the connect_tour_sales_staff tool "
                    "\n\nNote: The information you return to the user should match the results returned by the tool, and pay attention "
                    "to the chat history to keep the conversation seamless. Do not fabricate information. "
                    "If you respond with incorrect information, you may lose your job. On the contrary, if you do well, you will receive a bonus. "
                    "\n\nCurrent user information:\n<User>\n{user_info}\n</User>"
                    "\nCurrent time: {time}."
                    "\n\nIf the user needs help, and none of your tools are appropriate for it, then"
                    ' "CompleteOrEscalate" the dialog to the host assistant. Do not waste the user\'s time. Do not make up invalid tools or functions.',
                ),
                ("placeholder", "{messages}"),
            ]
        ).partial(time=datetime.now())
        
        tour_consultant_runnable = tour_consultant_prompt | llm.bind_tools(
            tour_consultant_tools + [CompleteOrEscalate]
        )

        tour_sale_prompt = ChatPromptTemplate.from_messages(
            [
                (
                    "system",
                    "You are a tour sales manager. " 
                    "Your job is to assign the right sales person to advise the customer in booking the perfect tour, "
                    "negotiating tour prices, applying promotional offers, and adjusting tour designs to meet budget requirements. " 
                    "\nAlways use the connect_tour_sales_staff tool to connect with the right sales person. " 
                    "\nAfter calling the connect_tour_sales_staff tool, please check the customer's phone number information "
                    "in current user information. If not, tactfully ask the customer to provide their phone number so that the consultant "
                    "can contact them directly. If you already have it, confirm with the customer whether the phone number is correct. "
                    "\n\nCurrent user information:\n<User>\n{user_info}\n</User>"
                    "\nCurrent time: {time}."
                    "\n\nIf the user needs help, and none of your tools are appropriate for it, then"
                    ' "CompleteOrEscalate" the dialog to the host assistant. Do not waste the user\'s time. Do not make up invalid tools or functions.',
                ),
                ("placeholder", "{messages}"),
            ]
        ).partial(time=datetime.now())
        
        tour_sale_runnable = tour_sale_prompt | llm.bind_tools(
            tour_sale_tools + [CompleteOrEscalate]
        )
    


        primary_assistant_prompt = ChatPromptTemplate.from_messages(
            [
                (
                    "system",
                    "Your name is {salesperson_name}. You are a customer support specialist for company {company_name}."    
                    "Your main role is to receive customer requests and assign them to the appropriate specialist to handle. "
                    "\nYou have rich knowledge in the field of tourism, including interesting destinations, travel tips, history of places, ... "
                    "With general questions, you can answer customers yourself." 
                    "\nIf the customer wants to find a tour, please leave it to the tour consultant." 
                    "\nIf a customer requests to book a tour, negotiate the tour price, ask about promotions or make changes to the tour design, "
                    "tour amenities or upgrade/downgrade hotel room class, etc., make adjustments to their needs"
                    "assign the task to the tour sales specialist. You cannot make these changes yourself." 
                    "\nIf the customer complains that the tour price is too high, ask the tour consultant to help find a similar tour at a lower price. "
                    "If the tour consultant cannot return a suitable result, forward the request to the tour sales specialist so that they can negotiate with the customer. " 
                    "\nOnly sales specialists are authorized to do this for users. "
                    "\nUsers do not know about the different professional assistants, so do not mention them; just quietly assign tasks through function calls. "
                    "\nProvide detailed information to customers and always double-check the database before concluding that information is unavailable. "
                    "When searching, be persistent. Expand your query if your first search does not return any results. "
                    "If your search does not return any results, expand your search before giving up. "
                    "\n\nCurrent user:\n<User>\n{user_info}\n</User>"
                    "\nCurrent time: {time}.",
                ),
                ("placeholder", "{messages}"),
            ]
        ).partial(time=datetime.now(), salesperson_name=salesperson_name, company_name=company_name)

        # primary_assistant_tools = [TavilySearchResults(max_results=1)]

        assistant_runnable = primary_assistant_prompt | llm.bind_tools(
            [
                ToTourConsultantAssistant,
                ToTourSaleAssistant, 
            ]
        )

        # DEFINE GRAPH --------------------------------------------------------------------------------
        builder = StateGraph(State)



        builder.add_node("fetch_user_info", user_info)
        builder.add_edge(START, "fetch_user_info")

        # Tour consultant assistant -----------------------------------------------------------------
        builder.add_node(
            "enter_tour_consultant",
            create_entry_node("Tour Consultant Assistant", "tour_consultant"),
        )
        builder.add_node("tour_consultant", Assistant(tour_consultant_runnable))
        builder.add_edge("enter_tour_consultant", "tour_consultant")

        builder.add_node(
            "tour_consultant_tools",
            create_tool_node_with_fallback(tour_consultant_tools),
        )

        builder.add_edge("tour_consultant_tools", "tour_consultant")
        builder.add_conditional_edges("tour_consultant", route_tour_consultant)

        # Tour sale assistant ------------------------------------------------------------------------
        builder.add_node(
            "enter_tour_sale",
            create_entry_node("Tour Sale Assistant", "tour_sales_manager"),
        )
        builder.add_node("tour_sales_manager", Assistant(tour_sale_runnable))
        builder.add_edge("enter_tour_sale", "tour_sales_manager")

        builder.add_node(
            "tour_sale_tools",
            create_tool_node_with_fallback(tour_sale_tools),
        )

        builder.add_edge("tour_sale_tools", "tour_sales_manager")
        builder.add_conditional_edges("tour_sales_manager", route_tour_sale)

        builder.add_node("leave_skill", pop_dialog_state)
        builder.add_edge("leave_skill", "primary_assistant")

        # Primary assistant
        builder.add_node("primary_assistant", Assistant(assistant_runnable))

        # The assistant can route to one of the delegated assistants,
        # directly use a tool, or directly respond to the user
        builder.add_conditional_edges(
            "primary_assistant",
            route_primary_assistant,
            {
                "enter_tour_consultant": "enter_tour_consultant",
                "enter_tour_sale": "enter_tour_sale",
                END: END,
            },
        )

        builder.add_conditional_edges("fetch_user_info", route_to_workflow)

        graph = builder.compile(
            checkpointer=memory,
        )

        return cls(graph=graph)
