import os
import json 
import pandas as pd
from langchain.graphs import Neo4jGraph

from dotenv import load_dotenv
load_dotenv()

from langchain.vectorstores.neo4j_vector import Neo4jVector
from langchain_openai import OpenAIEmbeddings
embeddings_model = "text-embedding-3-large"

data_file_path = "/home/anhcd/Products/travel-consultant-langgraph/data/datademo1.xlsx"

import pandas as pd

df = pd.read_excel(data_file_path)

# DB credentials
url = "bolt://localhost:7687"
username ="neo4j"
password = "pad12345"


graph = Neo4jGraph(
    url=url, 
    username=username, 
    password=password,
)

graph.query(
        "MATCH (n) DETACH DELETE n"
    )

for i, row in df.iterrows():
    for des in row["destination"].split("\n"):
        params = {
            "tour_id": row["tour_id"],
            "tour_name": row["tour_name"],
            "tour_description": row["tour_description"],
            "tour_info": row["tour_info"],
            "departure": row["departure"].strip(),
            "destination": des.strip(),
            "number_of_days": row["number_of_days"],
            "price_per_person": row["price_per_person"],
            "tour_schedule": row["tour_schedule"],
            "tour_policy": row["tour_policy"],
        }

        query = '''
            MERGE (t:Tour {id: $tour_id, name: $tour_name, price_per_person: $price_per_person, description: $tour_description, info: $tour_info, schedule: $tour_schedule, policy: $tour_policy})
            MERGE (ds:Destination {name: $destination})
            MERGE (t)-[:GO_TO]->(ds)
            MERGE (dp:Departure {name: $departure})
            MERGE (t)-[:START_FROM]->(dp)
            '''
        graph.query(query,params)
        print(params["tour_name"])

graph.query("DROP INDEX Tour IF EXISTS")
graph.query("DROP INDEX Destination IF EXISTS")
graph.query("DROP INDEX Departure IF EXISTS")

vector_index = Neo4jVector.from_existing_graph(
    OpenAIEmbeddings(model=embeddings_model),
    url=url,
    username=username,
    password=password,
    index_name='Tour',
    node_label="Tour",
    text_node_properties=['name', 'description'],
    embedding_node_property='embedding',
)
vector_index = Neo4jVector.from_existing_graph(
    OpenAIEmbeddings(model=embeddings_model),
    url=url,
    username=username,
    password=password,
    index_name='Destination',
    node_label="Destination",
    text_node_properties=['name'],
    embedding_node_property='embedding',
)
vector_index = Neo4jVector.from_existing_graph(
    OpenAIEmbeddings(model=embeddings_model),
    url=url,
    username=username,
    password=password,
    index_name='Departure',
    node_label="Departure",
    text_node_properties=['name'],
    embedding_node_property='embedding',
)

