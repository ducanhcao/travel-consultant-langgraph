import os
import logging, json
import time
from pydantic import BaseModel, Extra
from fastapi import FastAPI, Request, WebSocket, WebSocketDisconnect, HTTPException
from fastapi.templating import Jinja2Templates
from websockets.exceptions import ConnectionClosedOK
import uuid
from langchain_core.tools import tool
from graphrag import GraphStoreRetriever
from utils import merge_spaces
from constant import *
from langgraph.checkpoint.aiosqlite import AsyncSqliteSaver
from server.schemas import ChatResponse
from typing import Dict, List, Any

from graph_agent import GraphAgent

from dotenv import load_dotenv

_ = load_dotenv()

templates = Jinja2Templates(directory="templates")

app = FastAPI()

@app.get("/")
async def index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    config = {"configurable": {}}
    thread_id = str(uuid.uuid4())
    while True:
        data = await websocket.receive_text()
        message = json.loads(data)
        if 'userid' in message:
            config["configurable"] = {"user_id": message['userid'], "user_name": message['userid'], "user_phone_number": "0835238749", "thread_id": thread_id}
            break
        else:
            await websocket.send_text(f"Message received: {message['message']}")

    # Construct a response
    start_resp0 = ChatResponse(sender="bot", message="", type="start")
    await websocket.send_json(start_resp0.model_dump())

    memory = AsyncSqliteSaver.from_conn_string("/home/anhcd/Products/travel-consultant-langgraph/ahistorylanggraph.db")
    agent = GraphAgent.from_llm_with_memory(memory=memory)
    # ai_output_0 = rag_crew.run()
    ai_output_0 = "Xin kính chào quý khách! Tôi là chuyên gia tư vấn du lịch, sẵn sàng hỗ trợ quý khách tìm kiếm và lựa chọn những tour du lịch phù hợp nhất với nhu cầu và sở thích của mình, tôi có thể giúp gì cho bạn?" 
    resp0 = ChatResponse(sender="bot", message=ai_output_0, type="stream")
    await websocket.send_json(resp0.model_dump())
    # Send the end-response back to the client
    end_resp0 = ChatResponse(sender="bot", message="", type="end")
    await websocket.send_json(end_resp0.model_dump())

    while True:
        try:
            # Receive and send back the client message
            data = await websocket.receive_text()
            user_msg = json.loads(data)
            if 'message' in user_msg:
                user_msg = user_msg['message']
            else:
                resp = ChatResponse(
                    sender="bot",
                    message="Sorry, message not in data. Contact admin to fix.",
                    type="error",
                )
                await websocket.send_json(resp.model_dump())

            resp = ChatResponse(sender="human", message=user_msg, type="stream")
            await websocket.send_json(resp.model_dump())

            # Construct a response
            start_resp = ChatResponse(sender="bot", message="", type="start")
            await websocket.send_json(start_resp.model_dump())

            async for event in agent.graph.astream_events(
                {"messages": ("user", user_msg)}, config, stream_mode="values", version="v1"
            ):
                if event["event"] == "on_chat_model_stream":
                    content = event["data"]["chunk"].content
                    if content:
                        output_resp = ChatResponse(sender="bot", message=content, type="stream")
                        await websocket.send_json(output_resp.model_dump())
                elif event["event"] == "on_tool_start" or event["event"] == "on_tool_end": # TODO: Disable on production
                    print(event['name'], "\n", event["data"])

            # Send the end-response back to the client
            end_resp = ChatResponse(sender="bot", message="", type="end")
            await websocket.send_json(end_resp.model_dump())
        except WebSocketDisconnect:
            logging.info("WebSocketDisconnect")
            # TODO try to reconnect with back-off
            break
        except ConnectionClosedOK:
            logging.info("ConnectionClosedOK")
            # TODO handle this?
            break
        except Exception as e:
            logging.error(e)
            resp = ChatResponse(
                sender="bot",
                message="Sorry, something went wrong. Try again.",
                type="error",
            )
            await websocket.send_json(resp.model_dump())

class WebhookPayload(BaseModel):
    content: str
    class Config:
        extra = 'allow'

# Định nghĩa một endpoint cho webhook
@app.post("/webhook")
async def chatwoot_webhook(payload: dict):
    # if not payload.content:
    #     raise HTTPException(status_code=422, detail="Content field is required")
    print(payload)
    # print(event["conversation"])
    
    return {"status": "success"}