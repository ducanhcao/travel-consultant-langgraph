import os
import json 
import requests
from langchain.graphs import Neo4jGraph
from openai import OpenAI
from dotenv import load_dotenv
load_dotenv()

from langchain_community.vectorstores import Neo4jVector
from langchain_openai import OpenAIEmbeddings

from document_loaders import read_content_docx_and_doc, read_content_pdf
from constant import CONTENT_TYPE_MATCH_TMP_FILE, EXTRACT_INFO_TOUR_SYSTEM_PROMPT
from utils import check_keys_in_json

embeddings_model = "text-embedding-3-large"

# DB credentials
url = "bolt://localhost:7687"
username ="neo4j"
password = "pad12345"


graph = Neo4jGraph(
    url=url, 
    username=username, 
    password=password,
)

openai_client = OpenAI()

def extract_info_tour_from_file(file_path):
    if file_path.split(".")[-1] == 'doc' or file_path.split(".")[-1] == 'docx':
        content = read_content_docx_and_doc(file_path=file_path)
    elif file_path.split(".")[-1] == 'pdf':
        content = read_content_pdf(file_path=file_path)
    else:
        raise ValueError("Function extract_info_tour_from_file support doc/docs and pdf file")
    completion = openai_client.chat.completions.create(
        model="gpt-4o",
        temperature=0,
        response_format= {
            "type": "json_object"
        },
        messages=[
        {
            "role": "system",
            "content": EXTRACT_INFO_TOUR_SYSTEM_PROMPT
        },
        {
            "role": "user",
            "content": content
        }
        ]
    )
    completion.choices[0].message.content
    data = json.loads(completion.choices[0].message.content)
    if check_keys_in_json(data, ["tour_summary", "tour_itinerary", "tour_policy"]) == True:
        return data
    else:
        print("Warning: AI generated json data is incomplete with necessary information fields")
        return {'tour_summary': "", 'tour_itinerary': "", 'tour_policy': ""}

# Define the function to extract information based on template_file_type and template_url
def extract_info_tour(template_file_type, template_url):
    if template_file_type == 2:
        # Download the file
        response = requests.get(result[0]['template_url'])
        file_path = CONTENT_TYPE_MATCH_TMP_FILE[response.headers['Content-Type']]
        with open(file_path, "wb") as file:
            file.write(response.content)
        # Extract info
        info = extract_info_tour_from_file(file_path=file_path)

        return {
            "description": info['tour_summary'],
            "tour_itinerary": info['tour_itinerary'],
            "tour_policy": info['tour_policy']
        }
    else:
        return {
            "description": "",
            "tour_itinerary": "",
            "tour_policy": ""
        }

# Cypher query to find all Tour nodes with need_indexing = 1
query = """
MATCH (t:Tour)
WHERE t.need_indexing = 1 AND (t.status = 8194 OR t.status = 16386 OR t.status = 32770) AND t.template_file_type = 2
RETURN t.template_file_type AS template_file_type, t.template_url AS template_url, ID(t) AS node_id
"""

# Execute the query and iterate over the results
result = graph.query(query)
print(f"\n\nresult query: {result}\n\n")
for record in result:
    template_file_type = record["template_file_type"]
    template_url = record["template_url"]
    node_id = record["node_id"]

    # Extract information using the function
    tour_data = extract_info_tour(template_file_type, template_url)

    # Update the node with the extracted information
    update_query = """
    MATCH (t:Tour)
    WHERE ID(t) = $node_id
    SET t.description = $description,
        t.tour_itinerary = $tour_itinerary,
        t.tour_policy = $tour_policy,
        t.need_indexing = 0
    """

    graph.query(query=update_query, params={**tour_data, "node_id": node_id})

print("\n\nProperties updated for all Tour nodes with need_indexing = 1\n\n")


graph.query("DROP INDEX Tour IF EXISTS")
graph.query("DROP INDEX Destination IF EXISTS")
graph.query("DROP INDEX Departure IF EXISTS")

vector_index = Neo4jVector.from_existing_graph(
    OpenAIEmbeddings(model=embeddings_model),
    url=url,
    username=username,
    password=password,
    index_name='Tour',
    node_label="Tour",
    text_node_properties=['name', 'description'],
    embedding_node_property='embedding',
)
vector_index = Neo4jVector.from_existing_graph(
    OpenAIEmbeddings(model=embeddings_model),
    url=url,
    username=username,
    password=password,
    index_name='Destination',
    node_label="Destination",
    text_node_properties=['name'],
    embedding_node_property='embedding',
)
vector_index = Neo4jVector.from_existing_graph(
    OpenAIEmbeddings(model=embeddings_model),
    url=url,
    username=username,
    password=password,
    index_name='Departure',
    node_label="Departure",
    text_node_properties=['name'],
    embedding_node_property='embedding',
)

