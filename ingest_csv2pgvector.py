import os
import json 
import pandas as pd
from langchain.graphs import Neo4jGraph
from db import BASE_DB_DIR
from db.connect import create_db_connection
import psycopg2
from openai import OpenAI

from dotenv import load_dotenv
load_dotenv()

from langchain.vectorstores.neo4j_vector import Neo4jVector
from langchain_openai import OpenAIEmbeddings
embeddings_model = "text-embedding-3-large"
openai_client = OpenAI()

data_file_path = "/home/anhcd/Products/travel-consultant-langgraph/data/datademo1.xlsx"

import pandas as pd

df = pd.read_excel(data_file_path)

# Write text and embeddings to database
connection = create_db_connection()
cursor = connection.cursor()
try:
    for i, row in df.iterrows():
        for des in row["destination"].split("\n"):
            tour_id = row["tour_id"]
            tour_name = row["tour_name"]
            tour_description = row["tour_description"]
            tour_embedding_res = openai_client.embeddings.create(model=embeddings_model, input=tour_description)
            tour_embedding = tour_embedding_res.data[0].embedding
            tour_info = row["tour_info"]
            departure = row["departure"].strip()
            departure_embedding_res = openai_client.embeddings.create(model=embeddings_model, input=departure)
            departure_embedding = departure_embedding_res.data[0].embedding
            destination = des.strip()
            destination_embedding_res = openai_client.embeddings.create(model=embeddings_model, input=destination)
            destination_embedding = destination_embedding_res.data[0].embedding
            number_of_days = row["number_of_days"]
            price_per_person = row["price_per_person"]
            tour_schedule = row["tour_schedule"]
            tour_policy = row["tour_policy"]
            detail_url = row["detail_url"]

            cursor.execute(
                "INSERT INTO tours (tour_id, tour_name, tour_embedding, tour_description, tour_info, departure, \
                    departure_embedding, destination, destination_embedding, number_of_days, price_per_person, \
                        tour_schedule, tour_policy, detail_url) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                (tour_id, tour_name, tour_embedding, tour_description, tour_info, departure, \
                 departure_embedding, destination, destination_embedding, number_of_days, price_per_person, \
                    tour_schedule, tour_policy, detail_url)
            )

            print(row["tour_name"])

    connection.commit()
except (Exception, psycopg2.Error) as error:
    print("Error while writing to DB", error)
finally:
    if cursor:
        cursor.close()
    if connection:
        connection.close()
