import json 
from openai import OpenAI
from langchain_openai import OpenAIEmbeddings

from constant import SEARCH_TOUR_PGVECTOR_SYSTEM_PROMPT, PGDB_QUERY_MATCH
from db.connect import create_db_connection

class PgvectorRetriever:
    def __init__(self, openai_client, openai_embed_model):
        self.openai_client = openai_client
        self.openai_embed_model = openai_embed_model

    @classmethod
    def from_openai(cls, openai_client, openai_embed_model):
        return cls(openai_client, openai_embed_model)
    
    def define_query(self, prompt, model="gpt-4o"):
        completion = self.openai_client.chat.completions.create(
            model=model,
            temperature=0,
            response_format= {
                "type": "json_object"
            },
        messages=[
            {
                "role": "system",
                "content": SEARCH_TOUR_PGVECTOR_SYSTEM_PROMPT
            },
            {
                "role": "user",
                "content": prompt
            }
            ]
        )
        return completion.choices[0].message.content
    
    def create_embedding(self, text):
        embedding_res = self.openai_client.embeddings.create(model=self.openai_embed_model, input=text)
        return str(embedding_res.data[0].embedding)

    def query_db(self, json_data):
        json_data = json.loads(json_data)
        embed_params = {}
        query = "SELECT id, tour_name, tour_description, departures, destinations, adult_price, start_time, slots_available"
        total_similarity_element = []

        for key, val in json_data.items():
            if key in PGDB_QUERY_MATCH:
                embed_params[f"{key}_embedding"] = self.create_embedding(val)
                query += f', {PGDB_QUERY_MATCH[key]["similarity"]} AS {key}_similarity'
                total_similarity_element.append(f'{PGDB_QUERY_MATCH[key]["similarity"]} * {PGDB_QUERY_MATCH[key]["weight"]}')

        if len(total_similarity_element) > 0:
            total_similarity_sql = ", " + " + ".join(total_similarity_element) + " AS total_similarity"
            query += total_similarity_sql
            query += " FROM tours ORDER BY total_similarity desc LIMIT 3"

            connection = create_db_connection()
            cursor = connection.cursor()
            try:
                cursor.execute(query, embed_params)
                results = cursor.fetchall()
            except Exception as error:
                print("Error..", error)
            finally:
                cursor.close()
                connection.close()

        tours = ""
        if results:
            for result in results:
                tours += f"""Tour ID: {result[0]} 
Tour name: {result[1]} 
Tour description: {result[2]} 
Departure from: {result[3]} 
Visit places: {result[4]} 
Tour cost per person: {result[5]} 
Tour departs at: {result[6]} 
Number of seats available: {result[7]} 
-------------------------

"""
        
        return tours
    
    def query(self, user_needs: str) -> str:
        """Search for tours that suit your needs."""
        json_data = self.define_query(user_needs)
        print(json_data)
        result = self.query_db(json_data)
        print(result)
        return result

    
    def get_all_tour(self):
        return "No result"
    
    def get_tour_by_id(self, tour_id):
        return "No result"


# if __name__ == "__main__":
#     pgvector_retrieval = PgvectorRetriever.from_openai(openai_client=OpenAI(), openai_embed_model="text-embedding-3-large")
#     user_mess = "Có tour từ Hà Nội đi Bali dưới 10 triệu không?"
#     response = pgvector_retrieval.query(user_needs=user_mess)

