import json 
import logging

from langchain_community.graphs import Neo4jGraph
from openai import OpenAI
from constant import QUERY_GRAPHDB_SYSTEM_PROMPT, ENTITY_PROPERTIES_EMBED, ENTITY_RELATIONSHIP_MATCH, ENTITY_PROPERTIES_FILTER

class GraphStoreRetriever:
    def __init__(self, graph_db, openai_embed_model):
        self.openai_client = OpenAI()
        self.openai_embed_model = openai_embed_model
        self.graph_db = graph_db

    @classmethod
    def from_neo4j(cls, db_url, db_username, db_password, db_database, openai_embed_model):
        url = db_url
        username =db_username
        password = db_password
        graph_db = Neo4jGraph(
            url=url, 
            username=username, 
            password=password,
            database=db_database
        )

        return cls(graph_db, openai_embed_model)
    
    def define_query(self, prompt, model="gpt-4o"):
        completion = self.openai_client.chat.completions.create(
            model=model,
            temperature=0,
            response_format= {
                "type": "json_object"
            },
        messages=[
            {
                "role": "system",
                "content": QUERY_GRAPHDB_SYSTEM_PROMPT
            },
            {
                "role": "user",
                "content": prompt
            }
            ]
        )
        return completion.choices[0].message.content
    
    def create_embedding(self, text):
        result = self.openai_client.embeddings.create(model=self.openai_embed_model, input=text)
        return result.data[0].embedding
    
    def create_query(self, text):
        query_data = json.loads(text)
        # Creating embeddings
        embeddings_data = []
        for key, val in query_data.items():
            if key != 'Tour' and (key in ENTITY_PROPERTIES_EMBED or key in ENTITY_RELATIONSHIP_MATCH):
                embeddings_data.append(f"${key}Embedding AS {key}Embedding")
        query = "WITH " + ",\n".join(e for e in embeddings_data)
        # Matching products to each entity
        query += "\nMATCH (p:Tour)\nWHERE p.need_indexing = 0\nMATCH "
        match_data = []
        for key, val in query_data.items():
            if key != 'Tour' and key in ENTITY_RELATIONSHIP_MATCH:
                relationship = ENTITY_RELATIONSHIP_MATCH[key]
                match_data.append(f"(p)-[:{relationship}]->({key}Var:{key})")
        query += ",\n".join(e for e in match_data)
        similarity_data = []
        for key, val in query_data.items():
            if key != 'Tour' and key in ENTITY_RELATIONSHIP_MATCH:
                similarity_data.append(f"gds.similarity.cosine({key}Var.embedding, ${key}Embedding) AS {key}Similarity")
            if key != 'Tour' and key in ENTITY_PROPERTIES_EMBED:
                similarity_data.append(f"gds.similarity.cosine(p.embedding, ${key}Embedding) AS {key}Similarity")

        filter_data = []
        for key, val in query_data.items():
            if key != 'Tour' and key in ENTITY_PROPERTIES_FILTER :
                property_of_tour = ENTITY_PROPERTIES_FILTER[key]
                filter_data.append(f"p.{property_of_tour} < {val}")
        if len(filter_data) > 0:
            query += "\nWHERE "
            query += " AND ".join(e for e in filter_data)

        query += "\nWITH p,\n"
        query += ",\n".join(e for e in similarity_data)
        similarity_var = []
        for key, val in query_data.items():
            if key != 'Tour' and (key in ENTITY_PROPERTIES_EMBED or key in ENTITY_RELATIONSHIP_MATCH):
                similarity_var.append(f"{key}Similarity")
        query += "\nWITH p,\n"
        query += ",\n".join(e for e in similarity_var)
        query += ",\n("
        query += " + ".join(e for e in similarity_var)
        query += ") AS totalSimilarity\n"
        query += "ORDER BY totalSimilarity DESC\nLIMIT 3"
        query += "\nRETURN p, "
        query += ", ".join(e for e in similarity_var)
        query += ", totalSimilarity"
        return query


    def query_graph(self, response):
        embeddingsParams = {}
        query = self.create_query(response)
        # print(query)
        query_data = json.loads(response)
        for key, val in query_data.items():
            embeddingsParams[f"{key}Embedding"] = self.create_embedding(val)
        result = self.graph_db.query(query, params=embeddingsParams)
        return result 
    
    def query(self, user_needs: str) -> str:
        """Search for tours that suit your needs."""
        json_data = self.define_query(user_needs)
        print(json_data)
        result = self.query_graph(json_data)
        # print(result)
        return result

    
    def get_all_tour(self):
        query = "MATCH (p:Tour) WHERE p.need_indexing = 0 RETURN p"
        tours = self.graph_db.query(query)
        print(query)
        return tours
    
    def get_tour_by_id(self, tour_id):
        query = f"MATCH (p:Tour) WHERE p.need_indexing = 0 AND p.tour_id = {tour_id} RETURN p"
        tour = self.graph_db.query(query)
        print(query)
        return tour
