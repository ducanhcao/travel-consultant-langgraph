from fastapi import FastAPI, HTTPException, Form
from typing import Dict, Any
from pydantic import BaseModel
from langgraph.checkpoint.sqlite import SqliteSaver
from langchain_community.callbacks import get_openai_callback

from graph_agent import GraphAgent
from utils import caculate_tokens_and_price

memory = SqliteSaver.from_conn_string(":memory:")

class MessageRequest(BaseModel):
    human_message: str
    user_id: str
    user_name: str 
    user_phone_number: str
    thread_id: str

class MessageResponse(BaseModel):
    ai_message: str
    track_token: Any
    track_cost_usd: Any

app = FastAPI()

@app.post("/chat", response_model=MessageResponse) 
async def inference_message(human_message: str = Form(...), 
                            user_id: str = Form(...), 
                            user_name: str = Form(...), 
                            user_phone_number: str = Form(...), 
                            conversation_id: str = Form(...)):
    try:
        track_token = None
        track_cost = None
        agent = GraphAgent.from_llm_with_memory(memory=memory)
        config={"configurable": {
                    "user_id": user_id,
                    "user_name": user_name,
                    "user_phone_number": user_phone_number,
                    # Checkpoints are accessed by thread_id
                    "thread_id": conversation_id,
                }}
        with get_openai_callback() as cb:
            result = agent.graph.invoke(
                {"messages": ("user", human_message)}, config=config, stream_mode="values"
            )
            track_token=cb.total_tokens
            track_cost=cb.total_cost
        ai_message = result['messages'][-1].content
        # total_tokens, total_cost_vnd = caculate_tokens_and_price(ai_response=result)

    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return MessageResponse(ai_message=ai_message, track_token=track_token, track_cost_usd=track_cost)